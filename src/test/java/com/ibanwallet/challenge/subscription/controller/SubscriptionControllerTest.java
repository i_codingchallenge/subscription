package com.ibanwallet.challenge.subscription.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibanwallet.challenge.subscription.model.ErrorModel;
import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.service.SubscriptionService;
import com.ibanwallet.challenge.subscription.utils.TestObjects;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SubscriptionController.class)
class SubscriptionControllerTest {

    private static final String SUBSCRIPTION_API_URL = "/subscription";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private SubscriptionService subscriptionService;

    @Test
    void testSuccessCreate() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post(SUBSCRIPTION_API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(TestObjects.subscription()))
        ).andReturn();

        assertEquals(HttpStatus.CREATED.value(), mvcResult.getResponse().getStatus());

        verify(subscriptionService).create(any(SubscriptionModel.class));
    }

    @Test
    void testCreateNoBody() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post(SUBSCRIPTION_API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{}")
        ).andReturn();

        assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());

        Errors errors = mapper.readValue(mvcResult.getResponse().getContentAsString(Charset.forName("UTF-8")), Errors.class);

        assertNotNull(errors);
        assertNotNull(errors.getDate());
        assertEquals(3, errors.getErrors().size());
    }

    static class Errors {

        private LocalDateTime date;

        private List<ErrorModel> errors;

        public LocalDateTime getDate() {
            return date;
        }

        public void setDate(LocalDateTime date) {
            this.date = date;
        }

        public List<ErrorModel> getErrors() {
            return errors;
        }

        public void setErrors(List<ErrorModel> errors) {
            this.errors = errors;
        }
    }

}
