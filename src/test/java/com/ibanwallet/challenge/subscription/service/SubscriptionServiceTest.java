package com.ibanwallet.challenge.subscription.service;

import com.ibanwallet.challenge.subscription.exception.BusinessException;
import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import com.ibanwallet.challenge.subscription.persistance.repository.NewsletterRepository;
import com.ibanwallet.challenge.subscription.persistance.repository.SubscriptionRepository;
import com.ibanwallet.challenge.subscription.service.client.EmailService;
import com.ibanwallet.challenge.subscription.utils.TestObjects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@Transactional
public class SubscriptionServiceTest {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private NewsletterRepository newsletterRepository;

    @Mock
    private EmailService emailService;

    private SubscriptionService subscriptionService;

    @BeforeEach
    void setup(){
        subscriptionService = new SubscriptionService(subscriptionRepository, newsletterRepository, emailService);
    }

    @Test
    void createSubscription() throws BusinessException {
        Subscription subscription = subscriptionService.create(TestObjects.subscription());

        assertNotNull(subscription.getId());
        Mockito.verify(emailService).sendEmail(subscription.getEmail(),"Thank you for your subscription", "<h1>Thank you for your subscription!</h1>");
    }

    @Test
    void createSubscriptionWithNonExistentCampaign() {
        SubscriptionModel subscriptionModel = TestObjects.subscription();
        subscriptionModel.setCampaignId("NOT_EXISTS_1111222");

        BusinessException exception = assertThrows(BusinessException.class, () -> {
            subscriptionService.create(subscriptionModel);
        });

        assertEquals("The informed campaign does not exists, please inform a new one.", exception.getMessage());
    }

}
