package com.ibanwallet.challenge.subscription.utils;

import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import com.ibanwallet.challenge.subscription.types.GenderType;

import java.time.LocalDate;

public class TestObjects {

    public static SubscriptionModel subscription() {
        SubscriptionModel subscriptionModel = new SubscriptionModel();

        subscriptionModel.setBirthday(LocalDate.of(1990, 7, 18));
        subscriptionModel.setCampaignId("ABC-2020-111-1929229922-222");
        subscriptionModel.setConsent(true);
        subscriptionModel.setEmail("person1@test.com");
        subscriptionModel.setFirstName("Person 1");
        subscriptionModel.setGender(GenderType.M);

        return subscriptionModel;
    }

}
