package com.ibanwallet.challenge.subscription.model;

public class ErrorModel {

    private final String id;
    private final String message;

    public ErrorModel(String id, String message) {
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
