package com.ibanwallet.challenge.subscription.controller;

import com.ibanwallet.challenge.subscription.exception.BusinessException;
import com.ibanwallet.challenge.subscription.model.ErrorModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException e) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("date", LocalDateTime.now());
        body.put("errors", Arrays.asList(new ErrorModel("error", e.getMessage())));

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("date", LocalDateTime.now());

        if (e.getBindingResult().getFieldErrors() != null) {
            List<ErrorModel> errors = e.getBindingResult().getFieldErrors()
                    .stream()
                    .map(fieldError -> new ErrorModel(fieldError.getField(), fieldError.getDefaultMessage()))
                    .collect(Collectors.toList());
            body.put("errors", errors);
        }

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}
