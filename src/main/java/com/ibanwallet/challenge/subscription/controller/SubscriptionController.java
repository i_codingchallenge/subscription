package com.ibanwallet.challenge.subscription.controller;

import com.ibanwallet.challenge.subscription.exception.BusinessException;
import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import com.ibanwallet.challenge.subscription.service.SubscriptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/subscription")
@Api(value = "Subscription Services")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @PostMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Create subscriptions for campaign service")
    public Subscription create(@Valid @RequestBody SubscriptionModel subscription) throws BusinessException {
        return subscriptionService.create(subscription);
    }

}
