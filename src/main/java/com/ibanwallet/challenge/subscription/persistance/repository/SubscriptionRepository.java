package com.ibanwallet.challenge.subscription.persistance.repository;

import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {



}
