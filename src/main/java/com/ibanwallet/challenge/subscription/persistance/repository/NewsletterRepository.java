package com.ibanwallet.challenge.subscription.persistance.repository;

import com.ibanwallet.challenge.subscription.persistance.entity.Newsletter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsletterRepository extends JpaRepository<Newsletter, String> {
}
