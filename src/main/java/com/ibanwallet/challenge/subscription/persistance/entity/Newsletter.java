package com.ibanwallet.challenge.subscription.persistance.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Newsletter {

    @Id
    private String id;

    private String campaign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }
}
