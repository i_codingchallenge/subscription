package com.ibanwallet.challenge.subscription.service.client;

import com.ibanwallet.challenge.subscription.model.EmailModel;
import com.ibanwallet.challenge.subscription.types.EmailContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailService {

    @Value("${email.base.url}")
    private String apiEmailBaseUrl;

    @Value("${email.subscription.from.default}")
    private String defaultFrom;

    public void sendEmail(String to, String subject, String body) {
        RestTemplate restTemplate = new RestTemplate();

        EmailModel email = new EmailModel();

        email.setBody(body);
        email.setFrom(defaultFrom);
        email.setSubject(subject);
        email.setTo(to);
        email.setType(EmailContentType.HTML);

        restTemplate.postForEntity(new StringBuilder().append(apiEmailBaseUrl).append("/email").toString(), email, String.class);
    }

}
