package com.ibanwallet.challenge.subscription.service;

import com.ibanwallet.challenge.subscription.exception.BusinessException;
import com.ibanwallet.challenge.subscription.mapper.SubscriptionMapper;
import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import com.ibanwallet.challenge.subscription.persistance.repository.NewsletterRepository;
import com.ibanwallet.challenge.subscription.persistance.repository.SubscriptionRepository;
import com.ibanwallet.challenge.subscription.service.client.EmailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final NewsletterRepository newsletterRepository;
    private final EmailService emailService;

    public SubscriptionService(SubscriptionRepository subscriptionRepository, NewsletterRepository newsletterRepository, EmailService emailService) {
        this.subscriptionRepository = subscriptionRepository;
        this.newsletterRepository = newsletterRepository;
        this.emailService = emailService;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Subscription create(SubscriptionModel subscriptionModel) throws BusinessException {
        newsletterRepository.findById(subscriptionModel.getCampaignId()).orElseThrow(() ->
                new BusinessException("The informed campaign does not exists, please inform a new one."));

        Subscription subscription = subscriptionRepository.save(SubscriptionMapper.map(subscriptionModel));

        emailService.sendEmail(subscriptionModel.getEmail(), "Thank you for your subscription", "<h1>Thank you for your subscription!</h1>");

        return subscription;
    }

}
