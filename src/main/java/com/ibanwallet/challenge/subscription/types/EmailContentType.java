package com.ibanwallet.challenge.subscription.types;

public enum EmailContentType {

    TEXT, HTML

}
