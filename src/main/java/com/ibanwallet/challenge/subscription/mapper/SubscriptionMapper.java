package com.ibanwallet.challenge.subscription.mapper;

import com.ibanwallet.challenge.subscription.model.SubscriptionModel;
import com.ibanwallet.challenge.subscription.persistance.entity.Newsletter;
import com.ibanwallet.challenge.subscription.persistance.entity.Subscription;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionMapper {

    public static Subscription map(SubscriptionModel subscriptionModel) {
        Subscription subscription = new Subscription();

        subscription.setEmail(subscriptionModel.getEmail());
        subscription.setFirstName(subscriptionModel.getFirstName());
        subscription.setBirthday(subscriptionModel.getBirthday());
        subscription.setGender(subscriptionModel.getGender());
        subscription.setConsent(subscriptionModel.isConsent());

        if (StringUtils.isNotEmpty(subscriptionModel.getCampaignId())) {
            Newsletter newsletter = new Newsletter();
            newsletter.setId(subscriptionModel.getCampaignId());
            subscription.setNewsletter(newsletter);
        }

        return subscription;
    }

}
